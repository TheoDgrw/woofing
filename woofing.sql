-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`person`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`person` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT UNSIGNED NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `host` TINYINT NULL,
  `address` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`activity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`activity` (
  `id` INT UNSIGNED NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`type` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`subscription`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`subscription` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `date_start` TIMESTAMP NULL,
  `date_end` TIMESTAMP NULL,
  `valid` TINYINT NULL,
  `host_id` INT UNSIGNED NOT NULL,
  `person_id` INT UNSIGNED NOT NULL,
  `need_roof` TINYINT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_subscription_person1_idx` (`host_id` ASC) VISIBLE,
  INDEX `fk_subscription_person2_idx` (`person_id` ASC) VISIBLE,
  CONSTRAINT `fk_subscription_person1`
    FOREIGN KEY (`host_id`)
    REFERENCES `mydb`.`person` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_subscription_person2`
    FOREIGN KEY (`person_id`)
    REFERENCES `mydb`.`person` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`present`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`present` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `isPresent` TINYINT NOT NULL,
  `day` DATE NOT NULL,
  `subscription_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_present_subscription1_idx` (`subscription_id` ASC) VISIBLE,
  CONSTRAINT `fk_present_subscription1`
    FOREIGN KEY (`subscription_id`)
    REFERENCES `mydb`.`subscription` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`site`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`site` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `date_start` TIMESTAMP NULL,
  `date_end` TIMESTAMP NULL,
  `person_id` INT UNSIGNED NOT NULL,
  `capacity` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_site_person1_idx` (`person_id` ASC) VISIBLE,
  CONSTRAINT `fk_site_person1`
    FOREIGN KEY (`person_id`)
    REFERENCES `mydb`.`person` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`activity_has_person`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`activity_has_person` (
  `activity_id` INT UNSIGNED NOT NULL,
  `person_id` INT UNSIGNED NOT NULL,
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `level` INT NULL,
  INDEX `fk_activity_has_person_person1_idx` (`person_id` ASC) VISIBLE,
  INDEX `fk_activity_has_person_activity1_idx` (`activity_id` ASC) VISIBLE,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_activity_has_person_activity1`
    FOREIGN KEY (`activity_id`)
    REFERENCES `mydb`.`activity` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_activity_has_person_person1`
    FOREIGN KEY (`person_id`)
    REFERENCES `mydb`.`person` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`person_has_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`person_has_type` (
  `person_id` INT UNSIGNED NOT NULL,
  `type_id` INT UNSIGNED NOT NULL,
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  INDEX `fk_person_has_type_type1_idx` (`type_id` ASC) VISIBLE,
  INDEX `fk_person_has_type_person1_idx` (`person_id` ASC) VISIBLE,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_person_has_type_person1`
    FOREIGN KEY (`person_id`)
    REFERENCES `mydb`.`person` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_person_has_type_type1`
    FOREIGN KEY (`type_id`)
    REFERENCES `mydb`.`type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
